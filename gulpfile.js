var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
const shell = require('gulp-shell')
var reload      = browserSync.reload;
const packageImporter = require('node-sass-package-importer');



var src = {
    scss: 'themes/*.scss',
    css:  'public/themes',
    html: 'public/*.html'
};

// Compile sass into CSS
gulp.task('sass', function() {
  return gulp.src(src.scss)
      .pipe(sass({
        importer: packageImporter()
      }).on('error', sass.logError))
      .pipe(gulp.dest(src.css))
      .pipe(reload({stream: true}));
});

// Static Server + watching scss/html files
gulp.task('serve', gulp.series('sass', function() {

    browserSync.init({
        server: "./public",
        directory: true
    });

    gulp.watch(src.scss, gulp.series('sass'));
    gulp.watch(src.html).on('change', reload);
}));

gulp.task('marp', shell.task('yarn slides'));


gulp.task('default', gulp.parallel('serve', 'marp'));