module.exports = {
  allowLocalFiles: true,
  ogImage: process.env.URL && `${process.env.URL}/og-image.jpg`,
  themeSet: 'public/themes',
  url: process.env.URL,
}