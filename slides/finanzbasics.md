---
marp: true
_ßtheme: gaia
theme: dadvelopment
class: insta
---
<!-- class: brand -->
# dadvelopment Finanzbasics
---
<!-- class: "" -->
# Ziele
* Finanzen in den Griff kriegen
* Mit wenig Aufwand
* Grundlagen
* Ein besseres Gefühl für Geld bekommen
---
# Wie lange könntest du deinen Lebensstandard halten, wenn du jetzt aufhörst zu arbeiten?
### Wie viel Zeit kannst du überbrücken, bis Unterstützung / Sozialleistung überwiesen werden müssen?
---
# Grundlagen - Bewegungen im Geldsack I
* Einnahmen
* Ausgaben
---
<!-- class: invert -->
# Parkinsonsches Gesetz
* Ausgaben steigen stetig mit ihren Einnahmen
* Kaum ist Geld da, ist es auch schon wieder weg
---
# Die Dispofalle - Der erste Schritt in die Schulden
---
# Grundlagen - Bewegungen im Geldsack II
* Verbindlichkeiten
---
# Grundlagen - Bewegungen im Geldsack III
* Vermögenswerte

---
# Grundlagen - Alle Bewegungen im Großen Bild
* Einnahmen -> Ausgaben
* Gefahren, wenn nur oben bespielt wird
* Der wahre Geldfluss bei  Schulden
  * Einnahme - Schulden - Ausgabe
---
# Grundlagen - Inflation
---
# Finanziell Sicher I - Das Notfallkonto
* Bester Schutz vor dem Dispo
* UND Schritt 1 bei der Schuldenbekämfpung (siehe Dispiofalle)
--- 
# Licht ins Dunkel, Ordnung ins Chaos
## Erste Aktionen - Deine Einnahmen
* Liste alle deine regelmäßigen Einnahmen auf
* Brich sie auf monatlich herunter
* Nutze dafür deine Kontoauszüge
* Bilde eine Summe
---
## Erste Aktionen - Deine Ausgabe
* Liste deine Ausgaben auf
* Nutze deinen Kontoauszüge
* Nach Möglichkeit über die letzten drei Monate
* Brich es auf monatlich herunter
* Finde Sparpotentiale, spare nach Möglichkeit 10% sein
---
#Sparpotentiale
* Abos (Zeitschriften, Online Games, Netflix, Spotify, Disney+)
* Wiederkehrende Online Shops (Amazon, Lieferheld, Saturn, ...)
* Lotto
---
# Haushaltsbuchvorlage
---
# Das Parkinsonsche Gesetz
<!--
* Arbeit dehnt sich in dem Maße aus, in dem man Zeit dafür hat
* Ausgaben steigen mit den Einnahmen
* Die Ausgaben dehnen sich soweit aus, wie Geld zur Verfügung steht
 -->
---
# Exkurs: Wann sparen? Und warum?
## Erste Aktionen - Notfallkonto anlegen
* Eröffne ein Tagesgeldkonto
* Richte einen Dauerauftrag über die ersparten 10% aus der Lektion davor ein
* Ziel: 1000€
---
# Definition: Was ist ein Dauerauftrag
---
## Hast du Schulden? - Jetzt wird abgebaut I
### Sammele diese Informationen zu deinen Schulden
* Höhe
* Regelmäßige Rate
* Restbetrag
* Zinssatz
* ggf. Kontoführungsgebühr
* Schreibe es auf
---
## Hast du Schulden? - Jetzt wird abgebaut II
### Sammele diese Informationen zu deinen Schulden
* Kategorisiere die Schulden

| Konsumschulden | Wohnschulden | 
|-|-|
| Fernseher, Urlaub, Auto, Kreditkarte | Eigentumskredit, KfW-Förderung |
---
### Sortiere deine Konsumschulden - Wir bauen einen Schneeball
* Sortieren, kleinster Restbetrag zu erst
* ALLE Kredite / Verbindlichkeiten auf Minimalrate stellen
* Rate des kleinsten Kredites auf MAX
